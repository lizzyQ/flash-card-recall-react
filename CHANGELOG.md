Flashcard Recall React
Semantic-Version [SemVer.org](http://sem-ver.org)
>Please log changes made to source code here. Thank you.

# v0.0.1 -  9/16/2017

[code] - built `user.model`, `decks.model`, `table.component`, `flash-card.component`, and `game-instance-manager` 
[code] - Webpack 3 has been brought in with `webpack.config`
[refactor] - Changed `.ts` extentions all to `.tsx`

# v0.0.2 - 9/18/2017
[code] - built redux reducers and stores, `user.store` and `flashcard.store`  
[refactor] - reworked `game-instance-manager.component` to hold all the game logic and is a single point of truth
[code] - built `splash-screen.component`
[style] - added `main.style.scss` and `panel.style.scss` to the application data flow, need to run webpack to load it because its not parsed by anything. 
