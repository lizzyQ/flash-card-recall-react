import * as React from 'react';
import './App.css';
import {GameInstanceManagerComponent} from './components/game-instance-manager.component';

class App extends React.Component {
  render() {
    return (
      <GameInstanceManagerComponent/>
    );
  }
}

export default App;
