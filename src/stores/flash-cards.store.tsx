import {createStore} from 'redux'

export interface FlashCardsState {
	ready: boolean;
	deckSelected: string;

}

export interface Action {
	type: string;
	payload: {
		[name:string]: any
	};
}

export class FlashCardsStore {
	public store: any = createStore(flashCardReducer as any);
}


function flashCardReducer (action: Action = {type: '', payload: {}}, state: FlashCardsState): FlashCardsState {
	switch(action.type){
		case'DRAW_CARD':
			return { ...state, ...action.payload};
		default:
			return state;
	}

}

