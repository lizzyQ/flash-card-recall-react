import {AnyAction, createStore} from 'redux'
import {Action} from './flash-cards.store';
import {UserState} from '../components/pannel.component';


export class UserStore {
	public store: any = createStore(userReducer as any);
}

export function userReducer (action: Action = {type: '', payload: {}}, state: UserState): UserState {
	switch(action.type){
		case'WRONG_ANSWER':
			return { ...state, ...action.payload.user};
		case'RIGHT_ANSWER':
			return { ...state, ...action.payload.user};
		default:
			return state;
	}

}

