import * as React from 'react'
export interface HeaderComponent {
	props: {
		deck: any
	}
}

export class HeaderComponent extends React.Component {
	constructor(props: any){
		super(props)
	}
	render(){
		return(
			<div className="header">
				Current Deck Selected:
				<h1>{this.props.deck.deckTitle}</h1>
			</div>
		)
	}
}