import * as React from 'react';
import * as DECKS from '../models/decks.model';
import {FlashCardComponent} from './flash-card.component';
import {Observable} from 'rxjs/Observable';
import {FlashCard, FlashCardDeck} from '../models/decks.model';
import 'rxjs/Rx';


export interface TableComponent {
	deck: DECKS.FlashCardDeck;
	props: any;
	goal: number;
}

export interface GameState {
	misses: number;
	matches: number;
	streak: number;
	answered: number;
}

export class TableComponent extends React.Component {

	private CardQueue: DECKS.FlashCard[] = [];
	public state: GameState;
	public goal: number;

	constructor(
		public props: any,
	){
		super(props);
	}
	public componentDidMount() {
	}


	render(){

		return(
			<div className="card-table">
				<div className="header">
					Current Deck Selected:
					<h1>{this.props.deck.deckTitle}</h1>
					<h3>User Stats</h3>
				</div>

				<div className="panel">
					<div>
						Right: {this.state.matches}
					</div>
					<div>
						Answered: {this.state.answered}
					</div>
					<div>
						Missed: {this.state.misses}
					</div>
					<div>
						Current Streak: {this.state.streak}
					</div>
				</div>
			</div>

		)
	}

}

export default TableComponent