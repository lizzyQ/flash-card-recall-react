import * as React from 'react'

export interface UserState {
	matches: number;
	answered: number;
	misses: number;
	streak: number;
}
export class PannelComponent extends React.Component {
	state: UserState;

	constructor(props: any){
		super(props);
		this.state = this.initialState;
	}

	public initialState: UserState = {
		matches: 0,
		answered: 0,
		misses: 0,
		streak: 0,
	};
	render(){
		return(
			<div className="panel">
				<div>
					Right: {this.state.matches}
				</div>
				<div>
					Answered: {this.state.answered}
				</div>
				<div>
					Missed: {this.state.misses}
				</div>
				<div>
					Current Streak: {this.state.streak}
				</div>
			</div>
		)
	}
}