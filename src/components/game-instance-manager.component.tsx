import * as React from 'react';
import * as deck from '../models/decks.model';
import {FlashCard, FlashCardDeck} from '../models/decks.model';
import TableComponent, {GameState} from './table.component';
import FlashCardComponent from './flash-card.component';
import {PannelComponent} from './pannel.component';
import {HeaderComponent} from './header.component';
import {Observable} from 'rxjs/Observable';
const Rx = require('rxjs/Rx');
import {SplashScreenComponent} from './splash-screen.component';

// This is the smartest component in regards to the Flashcard Game,
// here would be business logic and state that effect the users progression through the drills

export class GameInstanceManagerComponent extends React.Component {
	cardQ: any = [];
	deck: FlashCardDeck = deck.DECKS('demo');
	goal: number;
	public state: GameState;
	constructor(props: any){
		super(props);
		this.state  = {
			misses: 0,
			matches: 0,
			streak: 0,
			answered: 0
		};

		this.handleSubmit = this.handleSubmit.bind(this);

	}

	componentDidMount(){
		this.initState();

	}

	onStart(){
		this.startTurn(this.drawCard());
	}

	handleSubmit(e: any): void {
		console.log('handleSubmit', e);
		if(this.verifyAnswer(e.val, e.card)){
			this.onMatch();
		} else {
			this.onMiss();
		}
		this.showAnswer(e.card)
	}

	renderHeader(){
		return <HeaderComponent deck={this.deck}></HeaderComponent>
	}

	renderPanel(){

		return <PannelComponent/>

	}

	renderLanding(){
		return <SplashScreenComponent />
	}

	renderCard(mode: string, card: FlashCard, callBack: () => void = () => {}){

		return (
			<FlashCardComponent card={card} mode={mode} onSubmit={callBack} />
		)
	}

	//need to clean
	public initState = () => {

		//set Queue
		let cardStack: FlashCard[] = [] as FlashCard[];
		for (let key of Object.keys(this.deck)) {
			if(key.match( /^[0-9]$/g)){
				console.log('hit', key);
				cardStack = [...cardStack, this.deck[key]];
			}
		}
		this.cardQ = cardStack;
		//set stats
		// this.stats = {...this.props.user.stats};

		//set winning conditions
		Rx.Observable.from(Object.keys(this.deck))
			.filter((val: any) => typeof val === 'number')
			.scan((acc: number[], value: any) => {
			if(+value){
				return acc.concat(+value)
			} else {
				return;
			}
			}, [])
			.debounce(() => Rx.Observable.timer(1000))
			.subscribe((keys: any) => this.goal = keys.length);
	};

	public startTurn(card: FlashCard){
		console.log('on turn', card);
		this.showQuestion(card);
		// this.verifyAnswer('', card);
		// 	this.onMatch();
		// 	this.onMiss();
		// this.addToQueue(card);
		// this.showAnswer(card);
	}

	private endTurn() {
		return(
			<div>
				<h1> Well Done! You have mastered that deck.</h1>
				<p>stats</p>
				<button>Again!</button>
				<button>Next Deck</button>
				<button>Done</button>
			</div>
		)
	}

	private checkForWin(): void {
		if (this.state['streak'] === this.goal) {
			this.endTurn()
		}
	}

	public showQuestion(card: FlashCard): any {
		return this.renderCard('question', card);
	};

	public showAnswer(card: FlashCard): {} {
		return this.renderCard('answer', card);
	};

	private addToQueue(card: FlashCard){
		this.cardQ = [...this.cardQ, card];
	}

	private verifyAnswer(val: string | number, card: FlashCard): Observable<boolean> {
		return Observable.of(card.answer)
			.filter((value: any) => {
				return value.toString()
					.includes('' + val);
			}).debounce(() => Observable.timer(1000))
	}

	private drawCard(): FlashCard{
		const card = this.cardQ[0];
		// Mutation on instance, ok??
		this.cardQ = this.cardQ.slice(1);
		return card;
	}

	private onMiss(){
		this.setState({streak: 0, misses: this.state.misses + 1});
		this.drawCard();
	}

	private onMatch(){
		this.setState(Object.assign({}, this.state, {streak: this.state.streak + 1, misses: this.state.misses + 1}))
		this.checkForWin();
		this.drawCard();
	}


	render(){
		return (
			<div className="gameInstManager">
				<h1>Flashcard Recall Drill<small>by RDev;)</small></h1>
				{this.renderHeader()}
				{this.renderCard('question', this.deck[0])}
				{this.renderPanel()}
			</div>
		)
	}

	//breakdown process after game is over

}
				//<Table deck={this.deck} />

export default GameInstanceManagerComponent;