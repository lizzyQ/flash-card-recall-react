import * as React from 'react';

export interface FlashCardComponent {
	props: any;
	mode: string;
	text: string;
}

export class FlashCardComponent extends React.Component {
	modelText: string;
	constructor(public props: any){
		super(props);
		console.log('prop', this.props);
	}

	onSubmit(e: any){
		console.log('hit', e);
	}

	renderCard(mode: string, text: string): any {
		switch(mode){
			case'question':
				return(
					<div className="flashCard">
						Question:<strong>{text}</strong>
						<hr/>
						<input value={this.modelText}/>
						<button onSubmit={() => console.log('onSubmit', event)} >Submit</button>
						{/*<button onClick={(event) => {this.props.onSubmit(event)}} >Submit</button>*/}
					</div>
				);
			case'answer':
				return(
					<div className="flashCard">
						Answer:<strong>{this.text}</strong>
						<button>Next Card</button>
					</div>
				);
			default:
				console.log('no card mode found');
				return;

		}
	}

	render(){
		return (
			<div className="flash-card">
				{this.renderCard(this.props.mode, this.props.card.question)}
			</div>
		)
	}
}

export default FlashCardComponent
