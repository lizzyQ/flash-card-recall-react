export interface UserModel {
	id: string,
	userName: string;
	email: string;
	mobile: string;
	stats: {
		[name:string]: any;
	}
}