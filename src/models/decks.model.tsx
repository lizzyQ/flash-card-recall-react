export interface FlashCard {
	cardId: string;
	deckId?: string;
	mode?: string;
	question?: string;
	answer?: string[];
	onSubmit: () => any;
	title: string;
	points?: number;
}

export interface FlashCardDeck {
	[key: number]: FlashCard,
	deckTitle: string,
	deckId: string,
}

export const DECKS = (deckName: string): any => {
	switch (deckName) {
		case'demo':
			return {
				deckId: 'rdev-coding-deck-00',
				deckTitle: 'Coding Trivia',
				0: {
					cardId: '0',
					question: 'What is functional programming?',
					answer: ['Functional programming is a paradigm that use functions to represent values.', 'usinging functions', 'functions provide information', 'a coding paradigm using functions', 'functions provide', 'functions relate', 'functions represent values', 'functions that represent values'],
				},
				1: {
					cardId: '1',
					question: 'What does CQRS stand for?',
					answer: ['Command Query Responsibility Segregation', 'Command, Query, Responsibility, Segregation']

				},
				2: {
					cardId: '3',
					question: 'What is the formula for Amortized Cost',
					answer: ['Cost(n Operations) / n', 'The sum of n actual costs divided by n']
				}

			};
		case'demo_2':
			return {
				deckId: 'rdev-coding-deck-01',
				deckTitle: 'Coding Trivia',
				0: {
					cardId: '3',
					title: 'f(null)',
					question: 'What is functional programming?',
					answer: ['Functional programming is a paradigm that use functions to represent values.', 'usinging functions', 'functions provide information', 'a coding paradigm using functions', 'functions provide', 'functions relate', 'functions represent values', 'functions that represent values'],
				},
				1: {
					id: '4',
					title: 'Did you mean Cars?',
					question: 'What does CQRS stand for?',
					answer: ['Command Query Responsibility Segregation', 'Command, Query, Responsibility, Segregation']

				},
				2: {
					id: '5',
					question: 'What is the formula for Amortized Cost',
					answer: ['Cost(n Operations) / n', 'The sum of n actual costs divided by n']
				}

			};
		default:
			return {message: 'No deck by that name.'} as any;
	}
};