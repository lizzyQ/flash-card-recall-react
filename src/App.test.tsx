import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {TableComponent} from './components/table.component';
import {DECKS} from './models/decks.model';
import {GameInstanceManagerComponent} from './components/game-instance-manager.component';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<GameInstanceManagerComponent />, div);
});

jest.dontMock('./components/table.component');

describe('Table Tests', () => {

	test('Table should provide a flash card object with a question, if given a deck', () => {
		expect( () => {
			const div = document.createElement('div');
			const deck = DECKS('demo');
			console.log(deck);

			ReactDOM.render(
		  <TableComponent />,
				div
			)
		}).toBe( () => {
			const question: any = document.getElementsByClassName('question');
			console.log(question)
		})
	})
});