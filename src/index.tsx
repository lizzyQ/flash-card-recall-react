import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import GameInstanceManagerComponent from './components/game-instance-manager.component';
import {UserStore} from './stores/user.store';
import {FlashCardsStore} from './stores/flash-cards.store';
import './styles/main.style.scss';

const userStore = new UserStore();
const unsubscribe = userStore.store.subscribe((update: any) => console.log('userStore Update',update));
const flashCardStore = new FlashCardsStore();
flashCardStore.store.subscribe((update: any) => console.log('flashCardStore update'));

unsubscribe();

ReactDOM.render(
  <GameInstanceManagerComponent />,
  document.getElementById('root')
);

registerServiceWorker();
